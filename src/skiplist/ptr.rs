use std::marker::PhantomData;
use std::mem;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::*;
use std::usize;

use skiplist::Node;

const UNLOCK_MASK: usize = usize::MAX - 1;
const LOCK_MASK: usize = 1;

#[derive(Debug)]
pub(crate) struct Ptr<T> {
    addr: AtomicUsize,
    _marker: PhantomData<T>,
}

impl<T> Ptr<T> {
    pub(crate) fn new(node: *mut Node<T>) -> Ptr<T> {
        Ptr {
            addr: AtomicUsize::new(node as usize),
            _marker: PhantomData,
        }
    }

    pub(crate) fn read(&self) -> Option<&Node<T>> {
        unsafe { mem::transmute(self.addr.load(Relaxed) & UNLOCK_MASK) }
    }

    pub(crate) unsafe fn read_unchecked(&self) -> &Node<T> {
        mem::transmute(self.addr.load(Relaxed) & UNLOCK_MASK)
    }

    pub(crate) unsafe fn lock(&self) {
        let addr = self.addr.load(Relaxed);
        let unlocked = addr & UNLOCK_MASK;
        let locked = unlocked | LOCK_MASK;
        while unlocked != self.addr.compare_and_swap(unlocked, locked, Acquire) { }
    }

    pub(crate) unsafe fn assign(&self, rhs: &Ptr<T>) {
        self.assign_addr(rhs.addr.load(Relaxed));
    }

    pub(crate) unsafe fn assign_addr(&self, addr: usize) {
        self.addr.store(addr, Relaxed);
    }
    
    pub(crate) unsafe fn unlock(&self) {
        let unlocked = self.addr.load(Relaxed) & UNLOCK_MASK;
        self.addr.store(unlocked, Release);
    }
}

impl<T> Drop for Ptr<T> {
    fn drop(&mut self) {
        if let Some(node) = self.read() {
            unsafe {
                node.drop_next();
                node.dealloc();
            }
        }
    }
}
