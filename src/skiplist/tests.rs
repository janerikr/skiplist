mod single_threaded {
    use skiplist::SkipList;

    fn skiplist() -> SkipList<usize> {
        let skiplist: SkipList<usize> = SkipList::new();
        skiplist.insert(110, <usize as Ord>::cmp);
        skiplist.insert(140, <usize as Ord>::cmp);
        skiplist.insert(100, <usize as Ord>::cmp);
        skiplist.insert(130, <usize as Ord>::cmp);
        skiplist.insert(120, <usize as Ord>::cmp);
        skiplist
    }

    #[test]
    fn len() {
        assert_eq!(skiplist().len(), 5);
    }

    #[test]
    fn is_empty() {
        let skiplist = SkipList::new();
        assert!(skiplist.is_empty());
        skiplist.insert(2, <usize as Ord>::cmp);
        assert!(!skiplist.is_empty());
    }

    #[test]
    fn builds_well_ordered_skiplist() {
        for _ in 0..10 {
            let skiplist = skiplist();
            skiplist.iter().zip(&[100, 110, 120, 130, 140]).for_each(|(elem, expected)| assert_eq!(elem, expected));
        }
    }

    #[test]
    fn find_works() {
        for _ in 0..10 {
            let skiplist = skiplist();
            assert!(skiplist.find(|elem| 100.cmp(elem)).is_some());
            assert!(skiplist.find(|elem| 110.cmp(elem)).is_some());
            assert!(skiplist.find(|elem| 120.cmp(elem)).is_some());
            assert!(skiplist.find(|elem| 130.cmp(elem)).is_some());
            assert!(skiplist.find(|elem| 140.cmp(elem)).is_some());
            assert!(skiplist.find(|elem| 150.cmp(elem)).is_none());
            assert!(skiplist.find(|elem| 10.cmp(elem)).is_none());
            assert!(skiplist.find(|elem| 125.cmp(elem)).is_none());
        }
    }

    #[test]
    fn iterate_by_value() {
        for _ in 0..10 {
            let skiplist = skiplist();
            skiplist.into_iter().zip(&[100, 110, 120, 130, 140]).for_each(|(elem, &expected)| assert_eq!(elem, expected));
        }
    }
}

mod multi_threaded {
    use std::sync::Arc;
    use std::thread;
    use skiplist::SkipList;

    #[test]
    fn insert_on_multiple_threads() {
        let skiplist = Arc::new(SkipList::new());
        let skiplist2 = skiplist.clone();
        let handle = thread::spawn(move || {
            for i in (100..200).filter(|x| x % 2 == 0) {
                skiplist2.insert(i, <usize as Ord>::cmp);
            }
        });
        for i in (100..200).filter(|x| x % 2 == 1) {
            skiplist.insert(i, <usize as Ord>::cmp);
        }
        handle.join().unwrap();
        for i in 100..200 {
            assert!(skiplist.find(|elem| i.cmp(elem)).is_some());
        }
    }
}
