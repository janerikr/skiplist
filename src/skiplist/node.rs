use std::cmp;
use std::heap::{Alloc, Heap, Layout};
use std::mem;
use std::ptr;
use std::slice;

use skiplist::Ptr;

#[repr(C)]
pub(crate) struct Node<T> {
    head: NodeHead<T>,
    lanes: Ptrs,
}

struct NodeHead<T> {
    elem: T,
    height: u8,
}

extern {
    type Ptrs;
}

impl<T> Node<T> {
    pub(crate) fn new(elem: T, height: usize) -> Ptr<T> {
        let layout = {
            let elem = Layout::new::<NodeHead<T>>();
            let lanes = Layout::array::<Ptr<T>>(height).unwrap();
            elem.extend(lanes).unwrap().0
        };

        unsafe {
            let alloc: *mut u8 = match Heap.alloc_zeroed(layout) {
                Ok(ptr) => ptr,
                Err(e)  => Heap.oom(e),
            };

            let node = alloc as *mut Node<T>;
            (*node).head = NodeHead { elem, height: height as u8 };

            Ptr::new(node)
        }
    }

    pub(crate) fn lanes(&self) -> &[Ptr<T>] {
        unsafe { 
            slice::from_raw_parts(&self.lanes as *const Ptrs as *const Ptr<T>,
                                  self.head.height as usize)
        }
    }

    pub(crate) fn set_next_at(&self, height: usize, next: &Node<T>) {
        unsafe { self.lanes().get_unchecked(height).assign_addr(next as *const _ as usize); }
    }

    pub(crate) fn elem(&self) -> &T {
        &self.head.elem
    }

    pub(crate) unsafe fn drop_next(&self) {
        drop(ptr::read(self.lanes().get_unchecked(0)));
    }

    pub(crate) unsafe fn dealloc(&self) {
        let size = mem::size_of::<NodeHead<T>>() + self.head.height as usize * mem::size_of::<Ptr<T>>();
        let align = cmp::max(mem::align_of::<NodeHead<T>>(), mem::align_of::<Ptr<T>>());
        let layout = Layout::from_size_align(size, align).unwrap();
        Heap.dealloc(self as *const Node<T> as *mut Node<T> as *mut u8, layout);
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn allocate_and_index() {
        let ptr: Ptr<usize> = Node::new(0, 8);
        let node = unsafe { ptr.read_unchecked() };
        assert!(node.lanes()[0].read().is_none());
        assert!(node.lanes()[7].read().is_none());
        for ptr in &node.lanes()[0..7] {
            assert!(ptr.read().is_none());
        }
    }
}
